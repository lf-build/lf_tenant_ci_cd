FROM registry.lendfoundry.com/base:core2 AS build-env
WORKDIR /app

COPY ./src/ /app/
RUN find . -name "global.json"  -type f -delete && find . -name "NuGet.config"  -type f -delete && find . -name "*.xproj"  -type f -delete

WORKDIR /app/LendFoundry.Tenant.Api
RUN eval "$CMD_RESTORE" && dotnet publish --verbosity normal -c Release -o out --no-restore

# Build runtime image
FROM microsoft/aspnetcore:2.0
WORKDIR /app/
COPY --from=build-env /app/LendFoundry.Tenant.Api/out .

LABEL lendfoundry.branch "LENDFOUNDRY_BRANCH"
LABEL lendfoundry.commit "LENDFOUNDRY_COMMIT"
LABEL lendfoundry.build "LENDFOUNDRY_BUILD_NUMBER"
LABEL lendfoundry.tag "LENDFOUNDRY_TAG"

ENTRYPOINT ["dotnet", "LendFoundry.Tenant.Api.dll"]
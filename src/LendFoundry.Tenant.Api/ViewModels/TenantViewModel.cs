﻿namespace LendFoundry.Tenant.Api.ViewModels
{
    public class TenantViewModel
    {
        public string Name { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public string Website { get; set; }

        public bool IsActive { get; set; }

    }
}

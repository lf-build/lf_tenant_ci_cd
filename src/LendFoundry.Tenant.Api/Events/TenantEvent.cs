﻿using LendFoundry.Tenant.Abstractions;

namespace LendFoundry.Tenant.Api.Events
{
    public class TenantEvent
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public ITenant Tenant { get; set; }

        public TenantEvent(string id)
        {
            Id = id;
        }

        public TenantEvent(string name, ITenant tenant)
        {
            Name = name;
            Tenant = tenant;
        }
    }
}

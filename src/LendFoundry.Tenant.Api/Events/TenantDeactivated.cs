﻿namespace LendFoundry.Tenant.Api.Events
{
    public class TenantDeactivated : TenantEvent
    {
        public TenantDeactivated(string id) : base(id)
        {
        }
    }
}

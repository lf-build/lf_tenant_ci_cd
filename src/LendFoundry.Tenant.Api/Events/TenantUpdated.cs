﻿using LendFoundry.Tenant.Abstractions;

namespace LendFoundry.Tenant.Api.Events
{
    public class TenantUpdated : TenantEvent
    {
        public TenantUpdated(string name, ITenant tenant) : base(name, tenant)
        {
        }
    }
}

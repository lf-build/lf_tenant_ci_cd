﻿namespace LendFoundry.Tenant.Api.Events
{
    public class TenantActivated : TenantEvent
    {
        public TenantActivated(string id) : base(id)
        {
        }
    }
}

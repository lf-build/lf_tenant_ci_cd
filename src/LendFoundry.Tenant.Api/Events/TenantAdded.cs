﻿using LendFoundry.Tenant.Abstractions;

namespace LendFoundry.Tenant.Api.Events
{
    public class TenantAdded : TenantEvent
    {
        public TenantAdded(string name, ITenant tenant) : base(name, tenant)
        {
        }
    }
}

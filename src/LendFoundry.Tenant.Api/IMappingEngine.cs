﻿namespace LendFoundry.Tenant.Api
{
    public interface IMappingEngine
    {
        void CreateMappings();
    }
}

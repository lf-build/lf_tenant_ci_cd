using System;
using LendFoundry.Foundation.Services.Settings;

namespace LendFoundry.Tenant.Api
{
    public static class Settings
    {
        private const string Prefix = "TENANT";

        public static string ServiceName { get; } = Prefix.ToLower();

        public static ServiceSettings EventHub { get; } = new ServiceSettings($"{Prefix}_EVENTHUB_HOST", "eventhub", $"{Prefix}_EVENTHUB_PORT");

        public static string Nats => Environment.GetEnvironmentVariable($"{Prefix}_NATS_URL") ?? "nats://nats:4222";

        public static string MongoConnectionString { get; } = Environment.GetEnvironmentVariable($"{Prefix}_MONGO_CONNECTION") ?? "mongodb://mongo:27017";

        public static string MongoDatabase { get; } = Environment.GetEnvironmentVariable($"{Prefix}_MONGO_DATABASE") ?? "tenant";
    }
}
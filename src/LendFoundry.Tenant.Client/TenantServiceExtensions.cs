using LendFoundry.Security.Tokens;
using LendFoundry.Foundation.Services;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace LendFoundry.Tenant.Client
{
    public static class TenantServiceExtensions
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public static IServiceCollection AddTenantService(this IServiceCollection services, string endpoint, int port = 5000, int cachingExpirationInSeconds=60)
        {
            services.AddTransient<ITenantServiceFactory>(p => new TenantServiceFactory(p, endpoint, port, cachingExpirationInSeconds));
            services.AddTransient(p => p.GetService<ITenantServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddTenantService(this IServiceCollection services, Uri uri, int cachingExpirationInSeconds = 60)
        {
            services.AddTransient<ITenantServiceFactory>(p => new TenantServiceFactory(p, uri, cachingExpirationInSeconds));
            services.AddTransient(p => p.GetService<ITenantServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddTenantService(this IServiceCollection services, int cachingExpirationInSeconds = 60)
        {
            Uri tenantUrl = new Uri(Environment.GetEnvironmentVariable("TENANT_URL") ?? "http://tenant:5000");
            services.AddTransient<ITenantServiceFactory>(p => new TenantServiceFactory(p, tenantUrl, cachingExpirationInSeconds));
            services.AddTransient(p => p.GetService<ITenantServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
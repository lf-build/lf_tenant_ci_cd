using System;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace LendFoundry.Tenant.Client
{
    public class TenantServiceFactory : ITenantServiceFactory
    {
        [Obsolete("Need to use the overloaded constructor with Uri")]
        public TenantServiceFactory(IServiceProvider provider, string endpoint, int port, int cachingExpirationInSeconds=60)
        {
            Provider = provider;
            Uri = new UriBuilder("http", endpoint, port).Uri;
            CachingExpirationInSeconds = cachingExpirationInSeconds;
        }
        public TenantServiceFactory(IServiceProvider provider, Uri uri, int cachingExpirationInSeconds = 60)
        {
            Provider = provider;
            Uri = uri;
        }

        private int CachingExpirationInSeconds { get; }
        private IServiceProvider Provider { get; }
        private Uri Uri { get; }

        public ITenantService Create(ITokenReader reader)
        {
            var client = Provider.GetServiceClient(reader, Uri);
            var handler = Provider.GetService<ITokenHandler>();
            return new TenantService(client, handler, reader, CachingExpirationInSeconds);
        }
    }
}
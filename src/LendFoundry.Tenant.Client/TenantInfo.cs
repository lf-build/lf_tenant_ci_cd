namespace LendFoundry.Tenant.Client
{
    public class TenantInfo
    {
        public TenantInfo()
        {
        }

        public TenantInfo(string id)
        {
            Id = id;
        }

        public string Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public string Website { get; set; }

        public Contact BusinessContact { get; set; }

        public Contact TechnicalContact { get; set; }

        public bool IsActive { get; set; }
    }
}
using System.Collections.Generic;

namespace LendFoundry.Tenant.Client
{
    public interface ITenantService
    {
        TenantInfo Current { get; }

        List<TenantInfo> GetActiveTenants();
    }
}
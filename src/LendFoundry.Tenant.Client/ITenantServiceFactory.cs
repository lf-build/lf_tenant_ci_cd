using LendFoundry.Security.Tokens;

namespace LendFoundry.Tenant.Client
{
    public interface ITenantServiceFactory
    {
        ITenantService Create(ITokenReader reader);
    }
}
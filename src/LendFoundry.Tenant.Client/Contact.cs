namespace LendFoundry.Tenant.Client
{
    public class Contact
    {
        public string Name { get; }

        public string Email { get; }

        public string Phone { get; }
    }
}
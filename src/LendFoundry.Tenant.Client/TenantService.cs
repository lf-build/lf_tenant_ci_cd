using System;
using System.Collections.Generic;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using RestSharp;
using System.Linq;
#if DOTNET2
using Microsoft.Extensions.Caching.Memory;
#else
using Microsoft.Framework.Caching.Memory;
# endif
namespace LendFoundry.Tenant.Client
{
    public class TenantService: ITenantService
    {
        private static readonly MemoryCache Cache = new MemoryCache(new MemoryCacheOptions());

        public TenantService(IServiceClient client, ITokenHandler handler, ITokenReader reader, int cachingExpirationInSeconds=60)
        {
            if (client == null)
                throw new ArgumentNullException(nameof(client));

            if (handler == null)
                throw new ArgumentNullException(nameof(handler));

            if (reader == null)
                throw new ArgumentNullException(nameof(reader));

            Client = client;
            Handler = handler;
            Reader = reader;
            CachingExpirationInSeconds = cachingExpirationInSeconds;
        }

        private IServiceClient Client { get; }

        private ITokenHandler Handler { get; }

        private ITokenReader Reader { get; }
        private int CachingExpirationInSeconds { get; }

        public TenantInfo Current
        {
            get
            {
                var rawToken = Reader.Read();
                var token = GetFromCacheOtherwise(rawToken, () =>
                {
                    return Handler.Parse(rawToken);
                });

                return token != null ? new TenantInfo(token.Tenant) : null;
            }
        }

        public List<TenantInfo> GetActiveTenants()
        {
            var request = new RestRequest(Method.GET);
            return Client.Execute<List<TenantInfo>>(request)
                .Where(t => t.IsActive)
                .ToList();
        }

        private IToken GetFromCacheOtherwise(string key, Func<IToken> function)
        {
            lock (Cache)
            {
                IToken response;
                if (!Cache.TryGetValue(key, out response))
                {
                    response = function();
                    if (CachingExpirationInSeconds > 0)
                    {
                        Cache.Set(key, response, new MemoryCacheEntryOptions
                        {
                            SlidingExpiration = TimeSpan.FromSeconds(CachingExpirationInSeconds)
                        });
                    }
                }

                return response;
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Tenant.Abstractions;

namespace LendFoundry.Tenant
{
    public class TenantService : ITenantService
    {
        public TenantService(ITenantRepository repository)
        {
            Repository = repository;
        }

        private ITenantRepository Repository { get; }


        public async Task<IEnumerable<ITenant>> GetAll()
        {
            return await Repository.GetAll();
        }

        public async Task<ITenant> Get(string id)
        {
            if (id == null)
                throw new ArgumentNullException(nameof(id));

            var entry= await Repository.Get(id);

            if (entry == null)
                throw new TenantNotFoundException($"{id} tenant not found");

            return entry;
        }

        public async Task<ITenant> Register(ITenant tenant)
        {
            if (tenant == null)
                throw new ArgumentNullException(nameof(tenant));

            if (tenant.Name == null)
                throw new ArgumentNullException(nameof(tenant.Name));

            if (tenant.Email == null)
                throw new ArgumentNullException(nameof(tenant.Email));

            if (tenant.Phone == null)
                throw new ArgumentNullException(nameof(tenant.Phone));

            if (tenant.Website == null)
                throw new ArgumentNullException(nameof(tenant.Website));
            
            return await Repository.Create(tenant);
        }

        public async Task<bool> Update(ITenant tenant)
        {
            if (tenant == null)
                throw new ArgumentNullException(nameof(tenant));

            if (tenant.Id == null)
                throw new ArgumentNullException(nameof(tenant.Id));

            if (tenant.Name == null)
                throw new ArgumentNullException(nameof(tenant.Name));

            if (tenant.Email == null)
                throw new ArgumentNullException(nameof(tenant.Email));

            if (tenant.Phone == null)
                throw new ArgumentNullException(nameof(tenant.Phone));

            if (tenant.Website == null)
                throw new ArgumentNullException(nameof(tenant.Website));

            var entry = await Get(tenant.Id);
            if (entry == null)
                throw new TenantNotFoundException($"{tenant.Id} tenant not found");

            entry.Name = tenant.Name;
            entry.Email = tenant.Email;
            entry.Phone = tenant.Phone;
            entry.Website = tenant.Website;
            return await Repository.Update(entry);
        }

        public async Task<bool> Activate(string id)
        {
            var tenant = await Get(id);
            tenant.IsActive = true;
            return await Repository.Update(tenant);
        }

        public async Task<bool> Deactivate(string id)
        {
            var tenant = await Get(id);
            tenant.IsActive = false;
            return await Repository.Update(tenant);
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Tenant.Abstractions;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;
using MongoDB.Driver;

namespace LendFoundry.Tenant.Mongo
{
    public class TenantRepository : ITenantRepository
    {
        static TenantRepository()
        {
            BsonClassMap.RegisterClassMap<Tenant>(map =>
            {
                map.AutoMap();
                map.MapIdField(m => m.Id).SetIdGenerator(StringObjectIdGenerator.Instance);
            });
        }

        public TenantRepository(IMongoConfiguration configuration)
        {
            var client = new MongoClient(configuration.ConnectionString);
            var database = client.GetDatabase(configuration.Database);

            Collection = database.GetCollection<Tenant>(typeof(Tenant).Name, new MongoCollectionSettings
            {
                AssignIdOnInsert = true
            });
        }

        private IMongoCollection<Tenant> Collection { get; }


        public async Task<IEnumerable<ITenant>> GetAll()
        {
            return await Task.Run(() => Collection.AsQueryable());
        }

        public async Task<ITenant> Get(string id)
        {
            return await Task.Run(() => Collection.AsQueryable().FirstOrDefault(t => t.Id == id));
        }

        public async Task<ITenant> Create(ITenant tenant)
        {
            var entry = Tenant.From(tenant);
            await Collection.InsertOneAsync(entry);
            return entry;
        }

        public async Task<bool> Update(ITenant tenant)
        {
            var result = await Collection.ReplaceOneAsync(t => t.Id == tenant.Id, Tenant.From(tenant));
            return result.IsAcknowledged && result.MatchedCount == result.ModifiedCount && result.ModifiedCount == 1;
        }
    }
}

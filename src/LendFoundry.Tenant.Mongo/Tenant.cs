﻿using LendFoundry.Tenant.Abstractions;

namespace LendFoundry.Tenant.Mongo
{
    public class Tenant : ITenant
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public string Website { get; set; }

        public bool IsActive { get; set; }

        public static Tenant From(ITenant tenant)
        {
            return new Tenant
            {
                Id = tenant.Id ?? tenant.Name?.ToLower().Trim().Replace(" ", "-").Replace("--", "-"),
                Name = tenant.Name,
                Email = tenant.Email,
                Phone = tenant.Phone,
                Website = tenant.Website,
                IsActive = tenant.IsActive
            };
        }
    }
}
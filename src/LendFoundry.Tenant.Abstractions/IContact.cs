namespace LendFoundry.Tenant.Abstractions
{
    public interface IContact
    {
        int Id { get; set; }
        string Name { get; }
        string Email { get; }
        string Phone { get; }
    }
}
﻿namespace LendFoundry.Tenant.Abstractions
{
    public interface ITenantServiceConfiguration
    {
        string Timezone { get; set; }

        string PaymentTimeLimit { get; set; }
    }
}
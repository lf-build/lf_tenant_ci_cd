﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Tenant.Abstractions
{
    public interface ITenantService
    {
        Task<IEnumerable<ITenant>> GetAll();

        Task<ITenant> Get(string id);

        Task<ITenant> Register(ITenant tenant);

        Task<bool> Update(ITenant tenant);

        Task<bool> Activate(string id);

        Task<bool> Deactivate(string id);
    }
}

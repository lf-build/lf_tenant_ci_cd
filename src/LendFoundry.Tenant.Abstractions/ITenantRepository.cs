using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Tenant.Abstractions
{
    /// <summary>
    /// Interface contract for tenant data persitence.
    /// </summary>
    public interface ITenantRepository
    {
        Task<IEnumerable<ITenant>> GetAll();

        Task<ITenant> Get(string id);

        Task<ITenant> Create(ITenant tenant);

        Task<bool> Update(ITenant tenant);
    }
}
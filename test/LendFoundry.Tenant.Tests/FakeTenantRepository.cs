﻿using LendFoundry.Tenant.Abstractions;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LendFoundry.Tenant.Tests
{
    public class FakeTenantRepository : ITenantRepository
    {
        private ConcurrentDictionary<string, ITenant> Tenants { get; set; } = new ConcurrentDictionary<string, ITenant>();

        public FakeTenantRepository(List<ITenant> tenants)
        {
            foreach (var item in tenants)
            {
                Tenants.TryAdd(item.Id, item);
            }
        }

        public Task<ITenant> Create(ITenant tenant)
        {
            var response = Tenants.TryAdd(tenant.Id, tenant);
            if (response == false)
            {
                throw new Exception("Unable to create tenant");
            }
            return Task.FromResult(tenant);
        }

        public Task<ITenant> Get(string id)
        {
            ITenant tenant;
            var response=Tenants.TryGetValue(id, out tenant);
            return Task.FromResult(tenant);
        }

        public Task<IEnumerable<ITenant>> GetAll()
        {
            IEnumerable<ITenant> tenants = new List<ITenant>(Tenants.Values);
            return Task.FromResult(tenants);
        }

        public Task<bool> Update(ITenant tenant)
        {
            var response= Tenants.TryRemove(tenant.Id, out ITenant tenantOld);
            if (response == false)
            {
                throw new Exception("Unable to create tenant");
            }
            response = Tenants.TryAdd(tenant.Id, tenant);
            if (response == false)
            {
                throw new Exception("Unable to create tenant");
            }
            return Task.FromResult(true);
        }
    }
}

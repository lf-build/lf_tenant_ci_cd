using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using Moq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace LendFoundry.Tenant.Client.Tests
{
    public class TenantServiceClientTests
    {
        private Mock<IServiceClient> ServiceClient { get; } = new Mock<IServiceClient>();
        private Mock<ITokenReader> TokenReader { get; } = new Mock<ITokenReader>();
        private Mock<ITokenHandler> TokenHandler { get; } = new Mock<ITokenHandler>();
        private int CacheDuration = 10;
        public TenantService GetTenantServiceClient()
        {
            return new TenantService(ServiceClient.Object, TokenHandler.Object, TokenReader.Object, CacheDuration);
        }

        [Fact]
        public void GetAllActiveTenantShouldReturnActiveTenantsOnly()
        {
            IRestRequest request;

            ServiceClient.Setup(s => s.Execute<List<TenantInfo>>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => request = r)
                .Returns(new List<TenantInfo>() {
                    new TenantInfo()
                    {
                        Id="tenant01",
                        IsActive=true
                    },
                    new TenantInfo()
                    {
                        Id="tenant02",
                        IsActive=true
                    },
                    new TenantInfo()
                    {
                        Id="inactive01",
                        IsActive=false
                    },
                    new TenantInfo()
                    {
                        Id ="tenant04",
                        IsActive=true
                    },
                    new TenantInfo()
                    {
                        Id="inactive02",
                        IsActive=false
                    }
                });

            var tenantService = GetTenantServiceClient();
            var allTenants = tenantService.GetActiveTenants();
            Assert.DoesNotContain(allTenants, (t) => t.Id.Contains("inactive"));
        }

        [Fact]
        public void CheckCurrentTenantReturnsValidTenantId()
        {
            TokenHandler.Setup(s => s.Parse(It.IsAny<string>())).Returns(new Token()
            {
                Tenant = "my-tenant"
            });
            TokenReader.Setup(s => s.Read()).Returns("my-tenant");

            var tenantService = GetTenantServiceClient();
            var tenantId = tenantService.Current.Id;
            Assert.Equal("my-tenant", tenantId);
            TokenHandler.Verify(s => s.Parse(It.IsAny<string>()), Times.Once);

        }

        [Fact]
        public void CheckCurrentTenantCacheWorks()
        {
            TokenHandler.Setup(s => s.Parse(It.IsAny<string>())).Returns(new Token()
            {
                Tenant = "my-tenant"
            });
            TokenReader.Setup(s => s.Read()).Returns("my-tenant");

            var tenantService = GetTenantServiceClient();
            var tenantId = tenantService.Current.Id;
            Assert.Equal("my-tenant", tenantId);
            TokenHandler.Verify(s => s.Parse(It.IsAny<string>()), Times.Once);
            TokenHandler.ResetCalls();

            tenantId = tenantService.Current.Id;
            Assert.Equal("my-tenant", tenantId);
            TokenHandler.Verify(s => s.Parse(It.IsAny<string>()), Times.Never);
            TokenHandler.ResetCalls();

            Task.Delay(TimeSpan.FromSeconds(CacheDuration + 10)).Wait();

            tenantId = tenantService.Current.Id;
            Assert.Equal("my-tenant", tenantId);
            TokenHandler.Verify(s => s.Parse(It.IsAny<string>()), Times.Once);
            TokenHandler.ResetCalls();

        }
    }
}

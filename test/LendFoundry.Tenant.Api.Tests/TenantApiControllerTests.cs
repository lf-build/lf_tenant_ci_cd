﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Tenant.Abstractions;
using LendFoundry.Tenant.Api.Controllers;
using LendFoundry.Tenant.Api.Events;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace LendFoundry.Tenant.Api.Tests
{
    public class TenantApiControllerTests
    {
        private Mock<ITenantService> TenantService { get; } = new Mock<ITenantService>();
        private Mock<ILogger> Logger { get; } = new Mock<ILogger>();
        private Mock<IEventHubClient> EventHubClient { get; set; } = new Mock<IEventHubClient>();

        private TenantController GetTenantController()
        {
            return new TenantController(TenantService.Object, Logger.Object, EventHubClient.Object);
        }

        [Fact]
        public void GetAllShouldReturnAllTenants()
        {
            var allTenants = new List<ITenant>() {
                new Tenant(){
                Email = "default@gmail.com",
                Id = "default",
                IsActive = true,
                Name = "default",
                Phone = "default",
                Website = "http://www.default.com"
                },
                new Tenant(){
                Email = "inactive@gmail.com",
                Id = "inactive",
                IsActive = true,
                Name = "inactive",
                Phone = "inactive",
                Website = "http://www.inactive.com"
                            },
                new Tenant(){
                Email = "inactive@gmail.com",
                Id = "test",
                IsActive = true,
                Name = "test",
                Phone = "inactive",
                Website = "http://www.inactive.com"
                }
            };

            var controller = GetTenantController();
            TenantService.Setup(s => s.GetAll()).ReturnsAsync(allTenants);
            var response = controller.GetAll().Result;
            Assert.IsType<OkObjectResult>(response);
            var tenantsObject = ((OkObjectResult)response).Value;
            Assert.IsType<List<ITenant>>(tenantsObject);
            List<ITenant> tenants = tenantsObject as List<ITenant>;
            Assert.Equal(3, tenants.Count);
        }

        [Fact]
        public void GetAllShouldReturnEmptyListWhenNoTenants()
        {
            var allTenants = new List<ITenant>();
            var controller = GetTenantController();
            TenantService.Setup(s => s.GetAll()).ReturnsAsync(allTenants);
            var response = controller.GetAll().Result;
            Assert.IsType<OkObjectResult>(response);
            var tenantsObject = ((OkObjectResult)response).Value;
            Assert.IsType<List<ITenant>>(tenantsObject);
            List<ITenant> tenants = tenantsObject as List<ITenant>;
            Assert.Empty(tenants);
        }

        [Fact]
        public void GetTenantShouldReturnTenant()
        {
            var defaultTenant = new Tenant()
            {
                Email = "default@gmail.com",
                Id = "default",
                IsActive = true,
                Name = "default",
                Phone = "default",
                Website = "http://www.default.com"
            };
            var controller = GetTenantController();
            TenantService.Setup(s => s.Get(It.IsAny<string>())).ReturnsAsync(defaultTenant);
            var response = controller.Get("default").Result;
            Assert.IsType<OkObjectResult>(response);
            var tenantObject = ((OkObjectResult)response).Value;
            Assert.IsAssignableFrom<ITenant>(tenantObject);
            ITenant tenant = tenantObject as ITenant;
            Assert.Equal(defaultTenant.Id, tenant.Id);
            Assert.Equal(defaultTenant.Name, tenant.Name);
        }

        [Fact]
        public void GetTenantShouldReturn404()
        {
            var controller = GetTenantController();
            TenantService.Setup(s => s.Get(It.IsAny<string>())).ThrowsAsync(new TenantNotFoundException());
            var response = controller.Get("default").Result;
            Assert.IsType<ErrorResult>(response);
            var tenantObject = (ErrorResult)response;
            Assert.Equal(404,tenantObject.StatusCode);
        }

        [Fact]
        public void GetTenantShouldReturn400()
        {
            var controller = GetTenantController();
            TenantService.Setup(s => s.Get(It.IsAny<string>())).ThrowsAsync(new ArgumentNullException());
            var response = controller.Get(null).Result;
            Assert.IsType<ErrorResult>(response);
            var tenantObject = (ErrorResult)response;
            Assert.Equal(400, tenantObject.StatusCode);
        }

        [Fact]
        public void RegisterTenantShouldReturn400WhenNullObjectPassed()
        {
            var controller = GetTenantController();
            TenantService.Setup(s => s.Register(null)).ThrowsAsync(new ArgumentNullException());
            var response = controller.Register(null).Result;
            Assert.IsType<ErrorResult>(response);
            var tenantObject = (ErrorResult)response;
            Assert.Equal(400, tenantObject.StatusCode);
            EventHubClient.Verify(s => s.Publish<TenantAdded>(nameof(TenantAdded), It.IsAny<TenantAdded>()), Times.Never);
        }

        [Fact]
        public void RegisterTenantShouldReturn400()
        {
            var controller = GetTenantController();
            TenantService.Setup(s => s.Register(It.IsAny<ITenant>())).ThrowsAsync(new ArgumentNullException());
            var response = controller.Register(new ViewModels.TenantViewModel()
            {
                Name=null
            }).Result;

            Assert.IsType<ErrorResult>(response);
            var tenantObject = (ErrorResult)response;
            Assert.Equal(400, tenantObject.StatusCode);
            EventHubClient.Verify(s => s.Publish<TenantAdded>(nameof(TenantAdded), It.IsAny<TenantAdded>()), Times.Never);
        }

        [Fact]
        public void RegisterTenantShouldSucceed()
        {
            var defaultTenant = new Tenant()
            {
                Email = "default@gmail.com",
                Id = "default",
                IsActive = true,
                Name = "default",
                Phone = "default",
                Website = "http://www.default.com"
            };

            var controller = GetTenantController();
            TenantService.Setup(s => s.Register(It.IsAny<ITenant>())).ReturnsAsync(defaultTenant);
            var response = controller.Register(new ViewModels.TenantViewModel()
            {
                Email = "default@gmail.com",
                IsActive = true,
                Name = "default",
                Phone = "default",
                Website = "http://www.default.com"
            }).Result;

            Assert.IsType<OkObjectResult>(response);
            var tenantObject = ((OkObjectResult)response).Value;
            Assert.IsAssignableFrom<ITenant>(tenantObject);
            ITenant tenant = tenantObject as ITenant;
            Assert.Equal(defaultTenant.Id, tenant.Id);
            Assert.Equal(defaultTenant.Name, tenant.Name);
            EventHubClient.Verify(s => s.Publish<TenantAdded>(nameof(TenantAdded), It.IsAny<TenantAdded>()), Times.Once);
        }


        [Fact]
        public void UpdateTenantShouldReturn400WhenNullObjectPassed()
        {
            var controller = GetTenantController();
            TenantService.Setup(s => s.Update(null)).ThrowsAsync(new ArgumentNullException());
            var response = controller.Update(null, null).Result;
            Assert.IsType<ErrorResult>(response);
            var tenantObject = (ErrorResult)response;
            Assert.Equal(400, tenantObject.StatusCode);
            EventHubClient.Verify(s => s.Publish<TenantAdded>(nameof(TenantAdded), It.IsAny<TenantAdded>()), Times.Never);
        }

        [Fact]
        public void UpdateTenantShouldReturn400()
        {
            var controller = GetTenantController();
            TenantService.Setup(s => s.Update(It.IsAny<ITenant>())).ThrowsAsync(new ArgumentNullException());
            var response = controller.Update("123", new ViewModels.TenantViewModel()
            {
                Name = null
            }).Result;

            Assert.IsType<ErrorResult>(response);
            var tenantObject = (ErrorResult)response;
            Assert.Equal(400, tenantObject.StatusCode);
            EventHubClient.Verify(s => s.Publish<TenantAdded>(nameof(TenantAdded), It.IsAny<TenantAdded>()), Times.Never);
        }

        [Fact]
        public void UpdateTenantShouldSucceed()
        {
            var controller = GetTenantController();
            TenantService.Setup(s => s.Update(It.IsAny<ITenant>())).ReturnsAsync(true);
            var response = controller.Update("default",new ViewModels.TenantViewModel()
            {
                Email = "default@gmail.com",
                IsActive = true,
                Name = "default",
                Phone = "default",
                Website = "http://www.default.com"
            }).Result;

            Assert.IsType<StatusCodeResult>(response);
            var statusCodeResult = ((StatusCodeResult)response);
            Assert.Equal(204, statusCodeResult.StatusCode);
            EventHubClient.Verify(s => s.Publish<TenantUpdated>(nameof(TenantUpdated), It.IsAny<TenantUpdated>()), Times.Once);
        }


        [Fact]
        public void ActivateTenantShouldReturn400()
        {
            var controller = GetTenantController();
            TenantService.Setup(s => s.Activate(It.IsAny<string>())).ThrowsAsync(new ArgumentNullException());
            var response = controller.Activate(null).Result;
            Assert.IsType<ErrorResult>(response);
            var tenantObject = (ErrorResult)response;
            Assert.Equal(400, tenantObject.StatusCode);
        }

        [Fact]
        public void ActivateTenantShouldReturn404()
        {
            var controller = GetTenantController();
            TenantService.Setup(s => s.Activate(It.IsAny<string>())).ThrowsAsync(new TenantNotFoundException());
            var response = controller.Activate("default").Result;
            Assert.IsType<ErrorResult>(response);
            var tenantObject = (ErrorResult)response;
            Assert.Equal(404, tenantObject.StatusCode);
        }

        [Fact]
        public void DeActivateTenantShouldReturn400()
        {
            var controller = GetTenantController();
            TenantService.Setup(s => s.Deactivate(It.IsAny<string>())).ThrowsAsync(new ArgumentNullException());
            var response = controller.Deactivate(null).Result;
            Assert.IsType<ErrorResult>(response);
            var tenantObject = (ErrorResult)response;
            Assert.Equal(400, tenantObject.StatusCode);
        }

        [Fact]
        public void DeActivateTenantShouldReturn404()
        {
            var controller = GetTenantController();
            TenantService.Setup(s => s.Deactivate(It.IsAny<string>())).ThrowsAsync(new TenantNotFoundException());
            var response = controller.Deactivate("default").Result;
            Assert.IsType<ErrorResult>(response);
            var tenantObject = (ErrorResult)response;
            Assert.Equal(404, tenantObject.StatusCode);
        }


    }
}
